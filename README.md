# Slim 3 API

A RESTful API Boilerplate built on Slim 3 that supports JSON/XML/HTML.

This is a skeleton application with enough boilerplate code to quickly get setup and working on Slim Framework 3.

###Features

- Vagrantfile with bootstrap for server setup
- PHP dotenv (and sample file)
- Respect\Validation
- Fractal
- RKA Content Type Renderer (supports JSON/XML/HTML)
- Monolog
- Swift Mailer
- Twig Template Engine
- JSON Web Tokens
- System Messages, Email, and UUID helper services

###Tests
I've created a test collection in Postman. Feel free to use my collection and the dev environment in the runner with the test data files located in this repository (app/tests/Postman).

It should be noted that by setting ```APP_ENV``` to ```dev``` within the .env file additional user information will be returned by some endpoints. This information (such as tokens) is useful for testing but should not be enabled for a production environment. In a production environment set the ```APP_ENV``` to ```prod```.

The .env file has other useful variables for testing purposes such as ```APP_DEBUG``` and ```APP_SENDMAIL``` which are self-explanatory.

[![Run in Postman](https://run.pstmn.io/button.svg)](https://documenter.getpostman.com/view/418351/77o3Lr1)

###Validation
Sometimes you want to validate, other times not, and sometimes a field is optional. In edge cases your field name may be different from your validator name. This validation middleware takes that all into consideration.

By setting the "validators" route argument you can specify any fields which you would like to validate. The validation rules are located in (app/src/Middleware/ValidationRules.php).
```
->setArguments(['validators' => ['id', 'email']])
```

The above example validates two fields, id and email, against the "id" and "email" validation rules.

For more complex validation supply the optional array as value to the parameter.
```
->setArguments(['validators' => [
    'id' => ['uuid'],
    'name' => [false, true],
    'password' => [false, true],
    'newPassword' => ['password', true, 'New Password']
]])
```

In the above example the "id" parameter is being validated using the "uuid" validation rule. The "name" parameter is optional. The "password" parameter is optional. And the "newPassword" is being validated using the "password" validation rules, is optional, and the translator is using the phrase "New Password" as the human-readable term for the field in any error messages.

For reference, the optional array uses the following signature. ```[string $validationRule, bool $optional, string $name]```

This middleware uses Respect\Validation so check out their documentation for further details: http://respect.github.io/Validation/

The validation middleware is loosely based on Davide Pastore's Slim-Validation. https://github.com/DavidePastore/Slim-Validation